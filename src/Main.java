

import com.paycenter.client.KindPay;
import com.paycenter.client.NoPropertyException;
import com.paycenter.client.PayCenterClient;
import com.paycenter.client.PayCenterResponse;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.String;import java.lang.System;import java.lang.Thread;import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Properties;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class Main {

    public static final String CONFIG_PROPERTIES = "paycenterclient.config.properties";

    public static void main(String[] args) throws CertificateException, NoPropertyException, NoSuchAlgorithmException, KeyStoreException, IOException, UnrecoverableKeyException, KeyManagementException {
        Properties props = loadProperties(CONFIG_PROPERTIES);
        PayCenterClient client = new PayCenterClient(props);
        System.out.println("make http request..");
        makeTestRequest(client);

        props.setProperty(PayCenterClient.PropName.SECURE_CONNECTION.name,"true");
        props.setProperty(PayCenterClient.PropName.PORT.name,"8088");
        client = new PayCenterClient(props);
        System.out.println("make https request..");
        makeTestRequest(client);
    }

    private static void makeTestRequest(PayCenterClient client) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
        System.out.println("make request for pay..");
        PayCenterResponse response = client.makePay(KindPay.QIWI, 100, "test");
        System.out.println("done request for pay. Responce:\n" + response);
        if (response.getPaymentDetails() != null) {
            System.out.println("make request for payment detals...");
            response = client.getPaymentDetails(response.getPaymentDetails().getPayId());
            System.out.println("done request for payment detals. Responce: \n" + response);
        }
    }


    private static Properties loadProperties(String fileName) {
        Properties properties = new Properties();
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName)) {
            properties.load(is);
            return properties;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
